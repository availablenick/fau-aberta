import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Carousel } from "react-responsive-carousel";
import {
  getDashboardCount,
  getDashboardCountByDep,
  getDashboard,
  getDashboardByDep,
  getDashboardKeywords,
  getDashboardMap,
  getDashboardNationalMap,
} from "../../../common/API";
import {
  Conteudo,
  DivCard,
  DivInfo,
  DivGraph,
  DivInfoText,
  DivDep,
  DivInfoProf,
  barColors,
  departmentColors,
  wordCloudColors,
  BarChartWrapper,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
} from "../../styles";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import BarChartContainer from "../../../common/charts/BarChartContainer";
import LineChartContainer from "../../../common/charts/LineChartContainer";
import NationalMapContainer from "../../../common/maps/NationalMapContainer";
import WordCloudContainer from "../../../common/keywords/WordCloudContainer";
import WorldMapContainer from "../../../common/maps/WorldMapContainer";
import SpinnerWrapper from "../wrappers/SpinnerWrapper";
import { setFilters } from "../../helpers";

function Department(props) {
  const { setMenuFilter, dep, depDescription, chefe, viceChefe, children } =
    props;

  useEffect(() => {
    setFilters(setMenuFilter, { [dep]: true }, (filter, newState) => {
      newState[filter] = false;
    });
  }, []);

  return (
    <Conteudo style={{ justifyContent: "start" }}>
      <DivCard>
        <DivInfo>
          <DivDep style={{ backgroundColor: departmentColors[dep] }}>
            {dep}
          </DivDep>
          <DivInfoProf>
            <span style={{ fontSize: 25 }}>{depDescription}</span>
            <span>Chefe</span>
            <span>{chefe}</span>
            <span>Vice-Chefe</span>
            <span>{viceChefe}</span>
          </DivInfoProf>
        </DivInfo>
        <DivGraph style={{ paddingLeft: "20px" }}>
          <Carousel
            showArrows
            showThumbs={false}
            showIndicators={false}
            showStatus={false}
            autoPlay
            infiniteLoop
            interval={5000}
          >
            <WordCloudWrapper>
              <WordCloudContainer
                colors={wordCloudColors[dep]}
                fetchData={() =>
                  getDashboardKeywords({ limit: 50, departamento: dep })
                }
                Loading={SpinnerWrapper}
              />
            </WordCloudWrapper>
            <WorldMapWrapper>
              <WorldMapContainer
                fetchData={() => getDashboardMap({ departamento: dep })}
              />
            </WorldMapWrapper>
            <NationalMapWrapper>
              <NationalMapContainer
                fetchData={() => getDashboardNationalMap({ departamento: dep })}
              />
            </NationalMapWrapper>
            <BarChartWrapper>
              <BarChartContainer
                labels={["FAU", dep]}
                colors={barColors.FAU}
                fetchData={async () => {
                  const FAUCount = await getDashboard();
                  const depCount = await getDashboardByDep({
                    departamento: dep,
                  });
                  return {
                    countByYearByCategoryByLabel: {
                      FAU: FAUCount,
                      [dep]: depCount,
                    },
                    categories: [
                      "Produção Artística",
                      "Produção Técnica",
                      "Produção Bibliográfica",
                      "Orientação",
                      "Bancas",
                      "Prêmios e Títulos",
                    ],
                  };
                }}
              />
            </BarChartWrapper>
            <LineChartWrapper>
              <LineChartContainer
                labels={[dep, "FAU"]}
                color={{
                  FAU: departmentColors.FAU,
                  [dep]: departmentColors[dep],
                }}
                fill={{ FAU: "-1", [dep]: "origin" }}
                fetchData={async () => {
                  const FAUCountByYear = await getDashboardCount({
                    ano_inicio: 1978,
                  });
                  const countByYearByDep = await getDashboardCountByDep({
                    ano_inicio: 1978,
                    departamento: dep,
                  });
                  return {
                    FAU: FAUCountByYear.FAU,
                    [dep]: countByYearByDep[dep],
                  };
                }}
              />
            </LineChartWrapper>
          </Carousel>
        </DivGraph>
        <DivInfoText>{children}</DivInfoText>
      </DivCard>
    </Conteudo>
  );
}

Department.propTypes = {
  chefe: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  dep: PropTypes.string.isRequired,
  depDescription: PropTypes.string.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
  viceChefe: PropTypes.string.isRequired,
};

export default Department;
