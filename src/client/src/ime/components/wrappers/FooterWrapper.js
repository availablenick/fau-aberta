import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { getLastUpdateDate } from "../../../common/API";
import Footer from "../../../common/misc/Footer";
import { instituteColor } from "../../styles";

function FooterWrapper() {
  const [lastDataUpdateDate, setLastDataUpdateDate] = useState(null);

  useEffect(() => {
    getLastUpdateDate().then((res) => {
      setLastDataUpdateDate(res);
    });
  }, []);

  return (
    <Footer
      lastDataUpdateDate={lastDataUpdateDate}
      lastDataUpdateNoticeColor={instituteColor}
    >
      <Info>
        <div>
          <b style={{ color: instituteColor }}>
            Instituto de Matemática e Estatística
          </b>
        </div>
        <div>R. do Matão, 1010 - Butantã, São Paulo - SP, 05508-090</div>
        <div>(11) 3091-6101</div>
      </Info>
      {/* CONFERIR cpq ime é isso mesmo */}
      <Info>
        <div>
          <b style={{ color: instituteColor }}>
            Comissão de Pesquisa - CPq IME/USP
          </b>
        </div>
        <div>
          contato:{" "}
          <a
            style={{ textDecoration: "none", color: "#000" }}
            href="mailto:cpqs-abertas@usp.br"
          >
            cpqs-abertas@usp.br
          </a>
        </div>
        <div>(11) 3091-4534</div>
      </Info>
    </Footer>
  );
}

const Info = styled.div`
  align-items: center;
  color: black;
  display: flex;
  flex-direction: column;

  > div {
    font-size: 12px;
    margin-bottom: 4px;
    text-align: center;
  }

  > a {
    color: white;
    font-size: 20px;
    text-decoration: none;
  }
`;

export default FooterWrapper;
