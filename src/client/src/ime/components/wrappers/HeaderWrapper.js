import React from "react";
import Header from "../../../common/misc/Header";
import IMELogo from "../../assets/images/ime.png";

function HeaderWrapper() {
  return (
    <Header
      title="IME Aberto"
      titleColor="#9D0A0E"
      subtitle="| Produção Intelectual do IME-USP"
      subtitleColor="#555555"
      fontFamily="Arial"
      instituteLogo={IMELogo}
      logoWidth={160}
      logoHeight={60}
    />
  );
}

export default HeaderWrapper;
