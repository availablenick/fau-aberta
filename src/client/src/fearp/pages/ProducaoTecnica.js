import React from "react";
import PropTypes from "prop-types";
import Activity from "../components/activity/Activity";
import {
  getProdTecCount,
  getProdTecCountByType,
  getProdTecKeywords,
  getProdTecMap,
  getProdTecNationalMap,
} from "../../common/API";
import { ProdTecDescription } from "../staticTexts";
import { setFilters } from "../helpers";

function ProducaoTecnica(props) {
  const { setMenuFilter } = props;

  const updateMenuEntry = () => {
    setFilters(setMenuFilter, { tecnica: true }, (filter, newState) => {
      newState[filter] = false;
    });
  };

  return (
    <Activity
      getCount={getProdTecCount}
      getCountByType={getProdTecCountByType}
      getKeywords={getProdTecKeywords}
      getMap={getProdTecMap}
      getNationalMap={getProdTecNationalMap}
      infoText={ProdTecDescription}
      type="Produção Técnica"
      updateMenuEntry={updateMenuEntry}
    />
  );
}

ProducaoTecnica.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default ProducaoTecnica;
