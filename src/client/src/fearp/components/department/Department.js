import React, { useEffect } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import {
  getDashboardCount,
  getDashboardCountByDep,
  getDashboard,
  getDashboardByDep,
  getDashboardKeywords,
  getDashboardMap,
  getDashboardNationalMap,
} from "../../../common/API";
import {
  Conteudo,
  DivCard,
  DivInfo,
  DivGraph,
  DivInfoText,
  DivInfoProf,
  barColors,
  departmentColors,
  wordCloudColors,
  instituteColor,
  highlightsFontFamily,
  departmentArrows,
  BarChartWrapper,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
} from "../../styles";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import BarChartContainer from "../../../common/charts/BarChartContainer";
import LineChartContainer from "../../../common/charts/LineChartContainer";
import NationalMapContainer from "../../../common/maps/NationalMapContainer";
import WordCloudContainer from "../../../common/keywords/WordCloudContainer";
import WorldMapContainer from "../../../common/maps/WorldMapContainer";
import CarouselWrapper from "../wrappers/CarouselWrapper";
import FaderWrapper from "../wrappers/FaderWrapper";
import { setFilters } from "../../helpers";

function Department(props) {
  const { setMenuFilter, dep, depDescription, chefe, viceChefe, children } =
    props;

  useEffect(() => {
    setFilters(setMenuFilter, { [dep]: true }, (filter, newState) => {
      newState[filter] = false;
    });
  }, []);

  return (
    <Conteudo style={{ justifyContent: "start" }}>
      <DivCard borderColor={departmentColors[dep]}>
        <DivInfo>
          <DivDep
            style={{ color: "#FFFFFF", backgroundColor: departmentColors[dep] }}
          >
            {dep}
          </DivDep>
          <DivInfoProf>
            <span style={{ fontSize: 25 }}>{depDescription}</span>
            <span>Chefe</span>
            <span>{chefe}</span>
            <span>Vice-Chefe</span>
            <span>{viceChefe}</span>
          </DivInfoProf>
        </DivInfo>

        <DivGraph style={{ paddingLeft: "20px" }}>
          <CarouselWrapper arrowImage={departmentArrows[dep]}>
            <WordCloudWrapper>
              <WordCloudContainer
                colors={wordCloudColors[dep]}
                fetchData={() =>
                  getDashboardKeywords({ limit: 50, departamento: dep })
                }
                Loading={FaderWrapper}
              />
            </WordCloudWrapper>
            <WorldMapWrapper>
              <WorldMapContainer
                color={departmentColors[dep]}
                fetchData={() => getDashboardMap({ departamento: dep })}
              />
            </WorldMapWrapper>
            <NationalMapWrapper>
              <NationalMapContainer
                color={departmentColors[dep]}
                fetchData={() => getDashboardNationalMap({ departamento: dep })}
              />
            </NationalMapWrapper>
            <BarChartWrapper>
              <BarChartContainer
                labels={["FEA-RP", dep]}
                colors={barColors.GRAYSCALE}
                fetchData={async () => {
                  const FEARPCount = await getDashboard();
                  const depCount = await getDashboardByDep({
                    departamento: dep,
                  });
                  return {
                    countByYearByCategoryByLabel: {
                      "FEA-RP": FEARPCount,
                      [dep]: depCount,
                    },
                    categories: [
                      "Produção Artística",
                      "Produção Técnica",
                      "Produção Bibliográfica",
                      "Orientação",
                      "Bancas",
                      "Prêmios e Títulos",
                    ],
                  };
                }}
              />
            </BarChartWrapper>
            <LineChartWrapper>
              <LineChartContainer
                labels={[dep, "FEA-RP"]}
                color={{
                  "FEA-RP": instituteColor,
                  [dep]: departmentColors[dep],
                }}
                fill={{ "FEA-RP": "-1", [dep]: "origin" }}
                fetchData={async () => {
                  const FEARPCountByYear = await getDashboardCount({
                    ano_inicio: 1978,
                  });
                  const countByYearByDep = await getDashboardCountByDep({
                    ano_inicio: 1978,
                    departamento: dep,
                  });
                  return {
                    "FEA-RP": FEARPCountByYear.FEARP,
                    [dep]: countByYearByDep[dep],
                  };
                }}
              />
            </LineChartWrapper>
          </CarouselWrapper>
        </DivGraph>

        <DivInfoText>
          <span className="title" style={{ fontSize: 25, fontWeight: "bold" }}>
            Histórico
          </span>
          {children}
        </DivInfoText>
      </DivCard>
    </Conteudo>
  );
}

Department.propTypes = {
  chefe: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  dep: PropTypes.string.isRequired,
  depDescription: PropTypes.string.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
  viceChefe: PropTypes.string.isRequired,
};

const DivDep = styled.div`
  align-items: center;
  background-position: center;
  background-position-y: -7.5px;
  background-repeat: no-repeat;
  background-size: 138px auto;
  border-radius: 50%;
  display: flex;
  font-family: ${highlightsFontFamily};
  font-size: 26px;
  font-weight: bold;
  height: 150px;
  justify-content: center;
  margin: 20px;
  width: 150px;
`;

export default Department;
