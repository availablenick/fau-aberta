import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
  Conteudo,
  DivCard,
  DivGraph,
  DivInfoProducao,
  DivInfoText,
  DivTitle,
  DivTotal,
  barColors,
  departmentColors,
  wordCloudColors,
  instituteColor,
  instituteArrow,
  BarChartWrapper,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
} from "../../styles";
import BarChartContainer from "../../../common/charts/BarChartContainer";
import LineChartContainer from "../../../common/charts/LineChartContainer";
import NationalMapContainer from "../../../common/maps/NationalMapContainer";
import WordCloudContainer from "../../../common/keywords/WordCloudContainer";
import WorldMapContainer from "../../../common/maps/WorldMapContainer";
import FaderWrapper from "../wrappers/FaderWrapper";
import CarouselWrapper from "../wrappers/CarouselWrapper";
import { getDashboard } from "../../../common/API";
import {
  createCountByCategoryByLabel,
  createCountByYearByDep,
  createFEARPCountByYear,
} from "../../helpers";

function Activity(props) {
  const {
    chartsToRemove,
    getCount,
    getCountByType,
    getKeywords,
    getMap,
    getNationalMap,
    infoText,
    type,
    updateMenuEntry,
  } = props;
  const [countByProductionType, setCountByProductionType] = useState(null);

  const chartsMap = {
    wordCloud: true,
    worldMap: true,
    nationalMap: true,
    barChart: true,
    instituteLineChart: true,
    depsLineChart: true,
  };

  chartsToRemove.forEach((chart) => {
    chartsMap[chart] = false;
  });

  useEffect(() => {
    updateMenuEntry();

    getDashboard().then((responsePayload) => {
      setCountByProductionType(responsePayload);
    });
  }, []);

  if (!countByProductionType) {
    return (
      <Conteudo
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <FaderWrapper />
      </Conteudo>
    );
  }

  const charts = {
    wordCloud: (
      <WordCloudWrapper>
        <WordCloudContainer
          colors={wordCloudColors.default}
          fetchData={() => getKeywords({ limit: 50 })}
          Loading={FaderWrapper}
        />
      </WordCloudWrapper>
    ),
    worldMap: (
      <WorldMapWrapper>
        <WorldMapContainer
          color={instituteColor}
          fetchData={() => getMap()}
          Loading={FaderWrapper}
        />
      </WorldMapWrapper>
    ),
    nationalMap: (
      <NationalMapWrapper>
        <NationalMapContainer
          color={instituteColor}
          fetchData={() => getNationalMap()}
          Loading={FaderWrapper}
        />
      </NationalMapWrapper>
    ),
    barChart: (
      <BarChartWrapper>
        <BarChartContainer
          labels={["RAD", "RCC", "REC"]}
          colors={barColors.GRAYSCALE}
          formatData={createCountByCategoryByLabel}
          fetchData={async () => {
            const countByYearByProductionTypeByDep = await getCountByType();
            const categories = Object.keys(
              Object.values(countByYearByProductionTypeByDep)[0]
            );
            return {
              categories,
              countByYearByCategoryByLabel: countByYearByProductionTypeByDep,
            };
          }}
          Loading={FaderWrapper}
        />
      </BarChartWrapper>
    ),
    instituteLineChart: (
      <LineChartWrapper>
        <LineChartContainer
          labels={["FEA-RP"]}
          color={{ "FEA-RP": instituteColor }}
          fill={{ "FEA-RP": "origin" }}
          formatData={createFEARPCountByYear}
          fetchData={() => getCount({ ano_inicio: 1978 })}
          Loading={FaderWrapper}
        />
      </LineChartWrapper>
    ),
    depsLineChart: (
      <LineChartWrapper>
        <LineChartContainer
          labels={["RAD", "RCC", "REC"]}
          color={{
            RAD: departmentColors.RAD,
            RCC: departmentColors.RCC,
            REC: departmentColors.REC,
          }}
          fill={{ RAD: false, RCC: false, REC: false }}
          formatData={createCountByYearByDep}
          fetchData={() => getCount({ ano_inicio: 1978 })}
          Loading={FaderWrapper}
        />
      </LineChartWrapper>
    ),
  };

  const carouselItems = [];
  Object.keys(chartsMap).forEach((chart) => {
    if (chartsMap[chart]) {
      carouselItems.push(charts[chart]);
    }
  });

  return (
    <Conteudo style={{ justifyContent: "start" }}>
      <DivCard style={{ border: "none" }}>
        <DivInfoProducao>
          <DivTitle>
            <span>{type}</span>
          </DivTitle>
          <DivTotal>
            <span style={{ fontSize: 100 }}>
              {Intl.NumberFormat().format(countByProductionType[type])}
            </span>
            <span>resultados</span>
          </DivTotal>
        </DivInfoProducao>
        <DivGraph>
          <CarouselWrapper arrowImage={instituteArrow}>
            {carouselItems}
          </CarouselWrapper>
        </DivGraph>
        <DivInfoText style={{ paddingTop: 0 }}>
          <span
            className="title"
            style={{ fontSize: 25, fontWeight: "bold", marginTop: -3.8 }}
          >
            Apresentação
          </span>
          <span>{infoText}</span>
        </DivInfoText>
      </DivCard>
    </Conteudo>
  );
}

Activity.propTypes = {
  chartsToRemove: PropTypes.array,
  getCount: PropTypes.func,
  getCountByType: PropTypes.func,
  getKeywords: PropTypes.func,
  getMap: PropTypes.func,
  getNationalMap: PropTypes.func,
  infoText: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  updateMenuEntry: PropTypes.func.isRequired,
};

Activity.defaultProps = {
  chartsToRemove: [],
  getCount: () => {},
  getCountByType: () => {},
  getKeywords: () => {},
  getMap: () => {},
  getNationalMap: () => {},
};

export default Activity;
