from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /producao-bibliografica/count
    url(r'^count$', views.count, name='producao_bibliografica-count'),
    # ex: /producao-bibliografica/aggregated-count
    url(r'^aggregated-count$', views.aggregated_count, name='producao_bibliografica-aggregated_count'),
    # ex: /producao-bibliografica/aggregated-count-by-type
    url(r'^aggregated-count-by-type$', views.aggregated_count_by_type, name='producao_bibliografica-count_by_type'),

    # ex: /producao-bibliografica/keywords
    url(r'^keywords$', views.keywords, name='producao_bibliografica-keywords'),
      # ex: /producao-bibliografica/aggregated-keywords
    url(r'^aggregated-keywords$', views.aggregated_keywords, name='producao_bibliografica-aggregated_keywords'),

    # ex: /producao-bibliografica/map
    url(r'^map$', views.map, name='producao_bibliografica-map'),
     # ex: /producao-bibliografica/aggregated-map
    url(r'^aggregated-map$', views.aggregated_map, name='producao_bibliografica-aggregated_map'),

    # ex: /producao-bibliografica/national-map
    url(r'^national-map$', views.national_map, name='producao_bibliografica-national_map'),
    # ex: /producao-bibliografica/aggregated-national-map
    url(r'^aggregated-national-map$', views.aggregated_national_map, name='producao_bibliografica-aggregated_national_map'),

    # ex: /producao-bibliografica/types-count
    url(r'^types-count$', views.types_count, name='producao_bibliografica-types_count'),
]
