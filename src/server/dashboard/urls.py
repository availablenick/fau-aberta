from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /dashboard/
    url(r'^$', views.index, name='dashboard-index'),
    # ex: /dashboard/aggregated-dashboard
    url(r'^aggregated-dashboard$', views.aggregated_dashboard, name='dashboard-aggregated_index'),

    # ex: /dashboard/search/
    url(r'^search$', views.search, name='dashboard-search'),

    # ex: /dashboard/count/
    url(r'^count$', views.count, name='dashboard-count'),
    # ex: /dashboard/aggregated-count/
    url(r'^aggregated-count$', views.aggregated_count, name='dashboard-aggregated_count'),

    # ex: /dashboard/countByDep/
    url(r'^countByDep$', views.countByDep, name='dashboard-countByDep'),

    # ex: /dashboard/countByDepTipo/
    url(r'^countByDepTipo$', views.countByDepTipo, name='dashboard-countByDepTipo'),

    # ex: /dashboard/indexByDep/
    url(r'^indexByDep$', views.indexByDep, name='dashboard-indexByDep'),

    # ex: /dashboard/keywords/
    url(r'^keywords$', views.keywords, name='dashboard-keywords'),

    # ex: /dashboard/map/
    url(r'^map$', views.map, name='dashboard-map'),
    # ex: /dashboard/aggregated-map/
    url(r'^aggregated-map$', views.aggregated_map, name='dashboard-aggregated_map'),

    # ex: /dashboard/national-map
    url(r'^national-map$', views.national_map, name='dashboard-national_map'),
    # ex: /dashboard/aggregated-national-map
    url(r'^aggregated-national-map$', views.aggregated_national_map, name='dashboard-aggregated_national_map'),

    # ex: /dashboard/last-updated
    url(r'^last-updated$', views.last_updated, name='dashboard-last-updated'),
]
