from django.http import JsonResponse
from django.core.mail import send_mail
import json
import os


def faq(request):
    if (request.method == "POST"):
        body = json.loads(request.body.decode('utf-8'))
        subject = f'Dúvida FAU Aberta - {body["params"]["subject"]}'
        message = f'Enviado por {body["params"]["name"]} ' \
            + f'({body["params"]["email"]}).\n\n\n{body["params"]["message"]}'
        try:
            send_mail(
                subject,
                message,
                os.environ.get("EMAIL_HOST_USER"),
                [os.environ.get("EMAIL_CPQS_ABERTAS"),
                    body["params"]["email"]],
                fail_silently=False
            )
        except Exception:
            return JsonResponse({'code': 500, 'result': {}}, status=500)

    return JsonResponse({'code': 200, 'result': {}})
