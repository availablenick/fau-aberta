import xml.etree.ElementTree as ET

from ...entities.colaboradores import insert_colaboradores_from_list
from ...entities.bancas import insert_bancas, insert_rel_pessoa_bancas
from ...entities.orientacao import insert_orientacao
from ...entities.participacao import insert_participacao, insert_rel_pessoa_participacao
from ...entities.pessoa import insert_pessoa
from ...entities.premios_titulos import insert_premios_titulos
from ...entities.producao_artistica import insert_producao_artistica, insert_rel_pessoa_prod_art
from ...entities.producao_bibliografica import insert_producao_bibliografica, insert_rel_pessoa_prod_bib
from ...entities.producao_tecnica import insert_producao_tecnica, insert_rel_pessoa_prod_tec

from .errors import messages

from ..xml.bancas import parse_bancas
from ..xml.orientacao import parse_orientacao
from ..xml.participacao import parse_participacao
from ..xml.pessoa import parse_pessoa
from ..xml.premios_titulos import parse_premios_titulos
from ..xml.producao_artistica import parse_producao_artistica
from ..xml.producao_bibliografica import parse_producao_bibliografica
from ..xml.producao_tecnica import parse_producao_tecnica

from ..parser import Parser


class XMLParser(Parser):
    def __init__(self, is_verbose, professors_by_dep, conn, cur):
        self.is_verbose = is_verbose
        self.professors_by_dep = professors_by_dep
        self.conn = conn
        self.cur = cur

    def get_lattes_id_from_filename(self, file):
        lattes_id = file[file.rindex('/')+1:-4]
        zeroes = 16 - len(lattes_id)
        return zeroes*"0" + lattes_id

    def has_correct_extension(self, file):
        return file.endswith('.xml')

    def is_well_formed(self, file):
        try:
            ET.parse(file)
        except ET.ParseError as error:
            lattes_id = self.get_lattes_id_from_filename(file)
            lineNumber, columnNumber = error.position
            print(f'Erro na interpretação do currículo {lattes_id}:\n{messages[error.code]} - linha {lineNumber}, coluna {columnNumber}')
            return False

        return True

    def insert_professor_information(self, file):
        lattes_id = self.get_lattes_id_from_filename(file)
        if self.is_verbose:
            print("\n------ Inserindo informações do docente de currículo {} ------\n".format(lattes_id))

        dep = self.professors_by_dep[lattes_id]
        print("DEPARTAMENTO")
        print(dep)

        tree = ET.parse(file)
        root = tree.getroot()

        professor = parse_pessoa(root, dep, lattes_id)
        insert_pessoa(professor, self.conn, self.cur)

    def insert_lattes_information(self, file):
        lattes_id = self.get_lattes_id_from_filename(file)
        if self.is_verbose:
            print("\n------ Inserindo informações de trabalhos do currículo {} ------\n".format(lattes_id))

        self.parse_and_insert_activities(file)

    def parse_and_insert_activities(self, file):
        tree = ET.parse(file)
        root = tree.getroot()
        lattes_id = self.get_lattes_id_from_filename(file)

        premios_titulos = parse_premios_titulos(root, lattes_id)
        for premio_titulo in premios_titulos:
            insert_premios_titulos(premio_titulo, self.conn, self.cur)

        orientacoes = parse_orientacao(root, lattes_id)
        for orientacao in orientacoes:
            insert_orientacao(orientacao, self.conn, self.cur)

        bancas = parse_bancas(root, lattes_id)
        for banca in bancas:
            id = insert_bancas(banca, self.conn, self.cur)
            dict = {'pessoa_id': lattes_id, 'bancas_id': id}
            insert_rel_pessoa_bancas(dict, self.conn, self.cur)

        producoes_tecnicas = parse_producao_tecnica(root, lattes_id)
        for producao in producoes_tecnicas:
            id = insert_producao_tecnica(producao, self.conn, self.cur)
            dict = {'pessoa_id': lattes_id, 'producao_id': id}
            insert_rel_pessoa_prod_tec(dict, self.conn, self.cur)
            insert_colaboradores_from_list(lattes_id, producao['autores'], self.conn, self.cur)

        producoes_artisticas = parse_producao_artistica(root, lattes_id)
        for producao in producoes_artisticas:
            id = insert_producao_artistica(producao, self.conn, self.cur)
            dict = {'pessoa_id': lattes_id, 'producao_id': id}
            insert_rel_pessoa_prod_art(dict, self.conn, self.cur)
            insert_colaboradores_from_list(lattes_id, producao['autores'], self.conn, self.cur)

        producoes_bibliograficas = parse_producao_bibliografica(root, lattes_id)
        for producao in producoes_bibliograficas:
            id = insert_producao_bibliografica(producao, self.conn, self.cur)
            dict = {'pessoa_id': lattes_id, 'producao_id': id}
            insert_rel_pessoa_prod_bib(dict, self.conn, self.cur)
            insert_colaboradores_from_list(lattes_id, producao['autores'], self.conn, self.cur)

        participacoes = parse_participacao(root, lattes_id)
        for participacao in participacoes:
            id = insert_participacao(participacao, self.conn, self.cur)
            dict = {'pessoa_id': lattes_id,
                    'participacao_id': id,
                    'titulo': participacao['titulo'],
                    'forma': participacao['forma'],
                    'tipo': participacao['tipo'],
                    }

            insert_rel_pessoa_participacao(dict, self.conn, self.cur)
