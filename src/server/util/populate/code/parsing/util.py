import json
import os
import re
from datetime import date


BASE_DIR = os.path.dirname(
    os.path.dirname(
        os.path.dirname(
            os.path.dirname(
                os.path.dirname(os.path.abspath(__file__))
            )
        )
    )
)


def normalize_string(s):
    s = s.lower()
    s = s.translate(str.maketrans("ãáàâéèêíìîõóòôúùûç", "aaaaeeeiiioooouuuc"))
    return s


with open(f'{BASE_DIR}/util/cities.json', 'r') as cities:
    estados = json.loads(cities.read())


with open(f'{BASE_DIR}/util/countries.json', 'r') as f:
    countries = list(json.loads(f.read()).keys())


def write_to_log(log_path, log_line):
    log = open(log_path, "a+")
    log.write(log_line)
    log.close()

def log_error_on_parse(title, authors, location, log_name):
    if not os.path.exists(f'{BASE_DIR}/util/populate/logs/{log_name}'):
        os.makedirs(f'{BASE_DIR}/util/populate/logs/{log_name}')

    today_date = date.today().strftime('%d-%m-%Y')

    log_path = f"{BASE_DIR}/util/populate/logs/{log_name}/{today_date}_parse_errors.log"
    log_line = f"{title};({authors});Localização mal formatada: {location}.\n"
    write_to_log(log_path, log_line)


def get_estado_from_cidade(details, location_keys, title, authors):
    for key in location_keys:
        if key not in details.attrib:
            continue

        cidade = normalize_string(details.attrib[key])
        estado = estados.get(cidade.strip(), "")

        if estado:
            return estado

        split_cidade = re.findall(r'[\w ]+', cidade)
        for element in split_cidade:
            estado = estados.get(element.strip().upper(), "")
            if estado:
                return estado
            else:
                tokens = element.split()
                for token in tokens:
                    estado = estados.get(token.strip().upper(), "")
                    if estado:
                        return estado

        log_error_on_parse(title, authors, cidade, "states")
        return ""

    log_error_on_parse(title, authors, 'LOCALIZAÇÃO NÃO ENCONTRADA', "states")
    return ""


def check_country_exists(country, production_title, authors):
    if normalize_string(country) not in countries:
        log_error_on_parse(production_title, authors, country, "countries")
