import psycopg2


# Makes an insert in orientacao table
def insert_orientacao(attr, conn=None, cur=None):
    """
    Inserts in orientacao.

    The insert_orientacao function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the orientacao table
    :type ev: dict of (<class 'bigint'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    try:
        cur.execute("SELECT id FROM orientacao WHERE pessoa_id = %s and aluno = %s and titulo = %s and ano = %s", [attr["pessoa_id"], attr["aluno"], attr["titulo"], attr["ano"]])
        get_id = cur.fetchall()
        id = None
        for row in get_id:
            id = int(row[0])

        if id is None:
            cur.execute('INSERT INTO orientacao (pessoa_id, aluno, ano, titulo, pais, instituicao, agencia_fomento, tipo_orientacao, tipo_pesquisa, natureza, idioma, flag, doi, curso) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)', [attr["pessoa_id"], attr["aluno"], attr["ano"], attr["titulo"], attr["pais"], attr["instituicao"], attr["agencia_fomento"], attr["tipo_orientacao"], attr["tipo_pesquisa"], attr["natureza"], attr["idioma"], attr["flag"], attr["doi"], attr["curso"]])
            conn.commit()
            cur.execute("SELECT id FROM orientacao WHERE pessoa_id = %s and aluno = %s and titulo = %s and ano = %s", [attr["pessoa_id"], attr["aluno"], attr["titulo"], attr["ano"]])
            get_id = cur.fetchall()
            for row in get_id:
                id = row[0]
        
        return id
    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()

    cur.close()
    conn.close()
