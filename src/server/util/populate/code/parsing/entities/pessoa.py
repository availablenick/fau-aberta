import psycopg2


# Makes an insert in pessoa table
def insert_pessoa(attr, conn=None, cur=None):
    """
    Inserts in pessoa.

    The insert_person function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the pessoa table
    :type ev: dict of (<class 'bigint'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    try:
        cur.execute("SELECT id_lattes FROM pessoa WHERE id_lattes = %s", [attr["id_lattes"]])
        get_id = cur.fetchall()
        id = None
        for row in get_id:
            id = int(row[0])

        if id is None:
            cur.execute('INSERT INTO pessoa (id_lattes, nome_completo, nome_citacao, nacionalidade, contato, departamento, cidade, estado, resumo) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)', [attr["id_lattes"], attr["nome_completo"], attr["nome_citacao"], attr["nacionalidade"], attr["contato"], attr["departamento"], attr["cidade"], attr["estado"], attr["resumo"]])
            conn.commit()
            cur.execute("SELECT id_lattes FROM pessoa WHERE id_lattes = %s", [attr["id_lattes"]])
            get_id = cur.fetchall()
            for row in get_id:
                id = row[0]

        return id
    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()

    cur.close()
    conn.close()


# Makes a select in pessoa table
def select_id_pessoa_by_name(nome, conn=None, cur=None):
    """
    Selects in pessoa.

    The select_id_pessoa function receives a name and select the id of the row
    from the database.

    :param nome: The name of the person
    :type nome: <class 'text'>
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    try:
        cur.execute("SELECT id_lattes FROM pessoa WHERE nome_completo = %s", [nome])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()

    cur.close()
    conn.close()
