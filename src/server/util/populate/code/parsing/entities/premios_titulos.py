import psycopg2


# Makes an insert in premios_titulos table
def insert_premios_titulos(attr, conn=None, cur=None):
    """
    Inserts in premios_titulos.

    The insert_premios_titulos function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the premios_titulos table
    :type ev: dict of (<class 'bigint'>, <class 'text'>, <class 'int'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    try:
        cur.execute("SELECT id FROM premios_titulos WHERE nome = %s and ano = %s", [attr["nome"], attr["ano"]])
        get_id = cur.fetchall()
        id = None
        for row in get_id:
            id = int(row[0])

        if id is None:
            cur.execute('INSERT INTO premios_titulos (pessoa_id, nome, ano, entidade) VALUES (%s, %s, %s, %s)', [attr["pessoa_id"], attr["nome"], attr["ano"], attr["entidade"]])
            conn.commit()
            cur.execute("SELECT id FROM premios_titulos WHERE nome = %s and ano = %s", [attr["nome"], attr["ano"]])
            get_id = cur.fetchall()
            for row in get_id:
                id = row[0]

        return id
    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()

    cur.close()
    conn.close()
